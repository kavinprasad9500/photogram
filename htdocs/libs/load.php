
<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include_once 'includes/Database.class.php';
include_once 'includes/User.class.php';
include_once 'includes/Session.class.php';
include_once 'includes/UserSession.class.php';
include_once 'includes/WebAPI.class.php';

global $__site_config;

/*
Note: Location of configuration
in lab : /home/user/phtogramconfig.json
in server: /var/www/photogramconfig.json
*/

// Session::start();
$wapi = new WebAPI();
$wapi->initiateSession();


function load_template($name)
{
    // include __DIR__ . "/../_templates/$name.php";
    include $_SERVER['DOCUMENT_ROOT'] . get_config('base_path')."_templates/$name.php"; 
}



// global $__site_config;
// Change Path on other server configuration 
// $__site_config = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/../kavin_photogramconfig.json');
// print($__site_config);

// $__site_config_path = dirname(is_link($_SERVER['DOCUMENT_ROOT']) ? readlink($_SERVER['DOCUMENT_ROOT']) : $_SERVER['DOCUMENT_ROOT']).'/project/kavin_photogramconfig.json';
// $__site_config = file_get_contents($__site_config_path);



function get_config($key, $default = null)
{
    global $__site_config;
    $array = json_decode($__site_config, true);
    if (isset($array[$key])) {
        return $array[$key];
    } else {
        return $default;
    }
}
