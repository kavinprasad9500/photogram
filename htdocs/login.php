<?php
include 'libs/load.php';
?>
<!DOCTYPE html>
<html lang="en">

<?php
load_template('_head');
?>

<body class="vw-100 vh-100 h-100 text-center text-white bg-dark-gradient sm-cover ">

  <?php
  load_template('_bgtheme');

  if (Session::isset('session_token')) {
    $token = Session::get('session_token');
    if (UserSession::authorize($token)) {
  ?>

      <script>
        window.location.href = "./index.php"
      </script>

  <?php
    } else {
      load_template('_login');
    }
  } else {
    load_template('_login');
  }
  load_template('_footer');
  load_template('_script');

  ?>



</body>

</html>