<?php

include 'libs/load.php';

?> 

<!doctype html>
<html lang="en">

<body>
    <?php
    if (Session::isset('session_token')) {
        $token = Session::get('session_token');
        if (UserSession::authorize($token)) {
    ?>

             <?php load_template('_photogram');   

        } else {
        ?>

        <script>window.location.href = "./login.php"</script>

        <?php
        }
    } else {
        ?>
            <script>window.location.href ="./login.php"</script>
        <?php
                }
                    ?>

</body>

</html>