<h1 class="pt-5 ">Photogram</h1>

<div class="container d-flex justify-content-center p-3">


    <div class="simple-black-blur-5 simple-black-border-blur-1 custom-width-1" style="border-radius: 10px;">

        <div class="card-header top-border-radius-1" style="background: rgba(255, 255, 255, 0.25);border-radius: 8px 8px 0px 0px;backdrop-filter: blur(50px);">
            <h4>Retrieve Password</h4>

        </div>

        <div class="card-body">
            <h5 class="p-2 mb-4">Enter the email address associated with your account and we'll send you to reset your
                password.</h5>

            <form autocomplete="on" action="" method="">
                <div class="form-floating mb-4 ">
                    <input type="email" class="form-control bg-transparent text-white" id="floatingEmail" placeholder="   " required>
                    <label for="floatingInput">Email address</label>
                </div>




                <button href="#" class="btn btn-success pull-right linear-green mb-3" style="background-color: #2DB65A; border-color: #2DB65A; width: 40%;">Continue</Button><br>



                <p>You can login now <a href="./login.html" style="color: #2DB65A; text-decoration: none;">here.</a></p>
            </form>
        </div>
    </div>
</div>]