<?php
$signup = false;
if (isset($_POST['username']) and isset($_POST['password']) and isset($_POST['email_address'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];
    $email = $_POST['email_address'];
    $phone = $_POST['phone'];
    $error = User::signup($username, $password, $email, $phone);
    $signup = true;
}

?>

<h1 class="pt-2 ">Welcome to Photogram</h1>

<div class="container d-flex justify-content-center p-3">

    <div class="simple-black-blur-5 simple-black-border-blur-1 custom-width-1" style="border-radius: 10px;">

        <div class="card-header top-border-radius-1" style="background: rgba(255, 255, 255, 0.25);border-radius: 8px 8px 0px 0px;backdrop-filter: blur(50px); ">

            <?php
            if ($signup) {
                if (!$error) {
            ?>
                    <h4>Success</h4>
        </div>

        <div class="card-body">
            <h5 class="m-3">Thanks for registered.</h5>
            <center>
                <lottie-player src="https://assets2.lottiefiles.com/packages/lf20_lk80fpsm.json" background="transparent" speed="1" style="width: 150px; height: 150px;" autoplay></lottie-player>
            </center>
            <h6 class="m-3">Your Account Has Been Succesfully Created. <br> Now you can login.</h6>

            <a href="./login.php"><Button type="submit" class="btn btn-success pull-right linear-green mb-3" style="background-color: #2DB65A; border-color: #2DB65A; width: 40%;">Continue</Button></a><br>

        </div>

    </div>
</div>

<?php

                } else {
                    echo ("<h6>$error</h6>");
                }
            } else {

?>
<h4>Please Register </h4>
</div>
<div class="card-body">
    <form autocomplete="on" method="post" action="register.php">

        <div class="form-floating mb-2 ">
            <input type="text" name="username" class="form-control bg-transparent text-white input-height" id="floatingUsername" placeholder=" " required>
            <label for="floatingInput">Username</label>
        </div>

        <div class="form-floating mb-2">
            <input type="text" name="email_address" class="form-control bg-transparent text-white" id="floatingauth" placeholder=" " required>
            <label for="floatingPassword">Email Address</label>
        </div>
        <div class="form-floating mb-2">
            <input type="text" name="phone" class="form-control bg-transparent text-white" id="floatingauth" placeholder=" " required>
            <label for="floatingPassword">Phone Number</label>
        </div>
        <div class="input-group form-floating mb-2">
            <input type="password" class="form-control bg-transparent text-white" id="floatingPassword" placeholder=" " pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" required style="border-right: none;">
            <span class="input-group-text bg-transparent text-white" style="border-top-right-radius: 5px; border-bottom-right-radius: 5px; width: 46px;" id="togglePassword"><i class="far fa-eye-slash" id="togglePasswordbtn"></i></span>
            <label for="floatingPassword">Password</label>
        </div>

        <div class="form-floating mb-2">
            <div class="input-group form-floating mb-2">
                <input type="password" name="password" class="form-control bg-transparent text-white" id="floatingConfirmPassword" placeholder=" " pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" onkeyup="validate_password()" required>
                <span class="input-group-text bg-transparent text-white" style="border-top-right-radius: 5px; border-bottom-right-radius: 5px; width: 46px;" id="toggleConfirmPassword"><i class="far fa-eye-slash" id="toggleConfirmPasswordbtn"></i></span>
                <label for="floatingPassword">Confirm Password</label>
            </div>

            <span id='message'></span>

        </div>


        <Button type="submit" class="btn btn-success pull-right linear-green mb-2 fw-bold" style="background-color: #2DB65A; border-color: #2DB65A; width: 40%;">Sign Up</Button><br>


        <small style="opacity: 0.8;">or Signup with</small>
        <div class="p-1">
            <a class="btn btn-square btn-outline-light rounded-circle me-1" href="#"><i class="fab fa-google" style="width: 15px; height: 15px;"></i></a>
            <a class="btn btn-square btn-outline-light rounded-circle me-1" href="#"><i class="fab fa-facebook-f" style="width: 15px;  height: 15px;" aria-hidden="true"></i></a>

        </div>

    </form>
</div>
<p>Already have an account <a href="./login.php" style="color: #2DB65A; text-decoration: none;" class="fw-bold">Login ?</a></p>
</div>
</div>

<?php
            }
?>