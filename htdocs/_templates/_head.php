<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="author" content="Kavin Prasad">
  <title>Photogram</title>
  <link href="./assets/css/reset.css" rel="stylesheet">
  <link href="./assets/css/style.css" rel="stylesheet">
  <!-- <link href="./assets/css/theme/pirateShip.css" rel="stylesheet"> -->
  <!-- <link href="./assets/css/theme/darkForest.css" rel="stylesheet"> -->
  <link href="./assets/css/theme/shipOcean.css" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.0-beta1/css/bootstrap.min.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.2/css/all.min.css" />
</head>