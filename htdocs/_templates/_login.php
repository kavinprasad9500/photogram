<h1 class="pt-4 ">Welcome back to Photogram</h1>
<h5 class="pb-0 ">The world without photography is useless!</h5>

<div class="container d-flex justify-content-center p-3">


  <div class="simple-black-blur-5 simple-black-border-blur-1 custom-width-1" style="border-radius: 10px;">

    <div class="card-header top-border-radius-1" style="background: rgba(255, 255, 255, 0.25);
      
      border-radius: 8px 8px 0px 0px;
      backdrop-filter: blur(50px); ">
      <h4>Please Sign in </h4>

    </div>
    <div class="card-body">
      <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
        <symbol id="check-circle-fill" viewBox="0 0 16 16">
          <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z" />
        </symbol>
        <symbol id="info-fill" viewBox="0 0 16 16">
          <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412-1 4.705c-.07.34.029.533.304.533.194 0 .487-.07.686-.246l-.088.416c-.287.346-.92.598-1.465.598-.703 0-1.002-.422-.808-1.319l.738-3.468c.064-.293.006-.399-.287-.47l-.451-.081.082-.381 2.29-.287zM8 5.5a1 1 0 1 1 0-2 1 1 0 0 1 0 2z" />
        </symbol>
      </svg>

      <?php


      $login = true;
      Session::set('mode', 'web');

      //TODO: Redirect to a requested URL instead of base path on login
      if (isset($_POST['email_address']) and isset($_POST['password']) and isset($_POST['fingerprint'])) {
        $email_address = $_POST['email_address'];
        $password = $_POST['password'];
        $fingerprint = $_POST['fingerprint'];
        $result = UserSession::authenticate($email_address, $password, $fingerprint);
        
        $login = false;
        // print($result);

        
      }
      if (!$login) {
        if ($result) {
      ?>

          <script>window.location.href = "./index.php"</script>
          
        <?php
        } else {
        ?>
          <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <svg class="bi flex-shrink-0 me-2" role="img" aria-label="Info:" style="width: 18px;height: 18px;">
              <use xlink:href="#info-fill" />
            </svg>
            <strong>Login Failed!</strong> Check your email or password.
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
          </div>

      <?php

        }
      }
      ?>



      <form autocomplete="on" method="post" action="login.php">
        <div class="form-floating mb-2 ">
          <input type="text" name="email_address" class="form-control bg-transparent text-white " id="floatingEmail" placeholder=" " required>
          <label for="floatingInput">Username or Email address</label>
        </div>

        <div class="input-group form-floating mb-2">
          <input type="password" name="password" class="form-control bg-transparent text-white" id="floatingPassword" placeholder=" " pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" required>
          <span class="input-group-text bg-transparent text-white" style="border-top-right-radius: 5px; border-bottom-right-radius: 5px;width: 46px;" id="togglePassword"><i id="togglePasswordbtn" class="far fa-eye-slash"></i></span>
          <label for="floatingPassword">Password</label>
          <input type="hidden" class="form-control" id="fingerprint" name="fingerprint" required>
        </div>
        <input type="hidden" class="form-control" id="fingerprint">

        <div class="mb-3">
          <input type="checkbox" class="form-check-input " id="remembercheck">
          <label class="form-check-label" for="exampleCheck1"> Remember me </label>

        </div>


        <button href="#" type="submit" class="btn btn-success pull-right linear-green mb-3 fw-bold" style="background-color: #2DB65A; border-color: #2DB65A; width: 40%;">Sign in</Button><br>


        <small style="opacity: 0.8;">or Login with</small>
        <div class="p-3">
          <a class="btn btn-square btn-outline-light rounded-circle me-1" href="#"><i class="fab fa-google" style="width: 15px; "></i></a>
          <a class="btn btn-square btn-outline-light rounded-circle me-1" href="#"><i class="fab fa-facebook-f" aria-hidden="true" style="width: 15px;"></i></a>

        </div>
        <a href="./forgot.html" style="color: #2DB65A; text-decoration: none;" class="fw-bold">Forgot Password?</a>

      </form>
      
    </div>
    <p>Don't have an account <a href="./register.php" style="color: #2DB65A; text-decoration: none;" class="fw-bold">Register</a> ?
    </p>
  </div>
</div>
<script>
  // Initialize the agent at application startup.
  const fpPromise = import('https://openfpcdn.io/fingerprintjs/v3')
    .then(FingerprintJS => FingerprintJS.load())

  // Get the visitor identifier when you need it.
  fpPromise
    .then(fp => fp.get())
    .then(result => {
      // This is the visitor identifier:
      const visitorId = result.visitorId;
      $("#fingerprint").val(visitorId);
    })
</script>