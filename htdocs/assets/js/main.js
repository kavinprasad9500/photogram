var scene = document.getElementById("scene");
var parallaxInstance = new Parallax(scene, {
  scalarX: 2,
  scalarY: 2,
});
parallaxInstance.friction(1, 1);

// Check the Password and Confirm Password are Same
var validate_password = function () {
  if (
    document.getElementById("floatingPassword").value ==
    document.getElementById("floatingConfirmPassword").value
  ) {
    document.getElementById("message").innerHTML =
      '<i class="fa-solid fa-circle-check" style="color: #2DB65A;"></i> Password Matched';
  } else {
    document.getElementById("message").innerHTML =
      '<i class="fa-solid fa-circle-xmark" style="color: red;"></i> Password does not Matched';
  }
};

// Password Toggle Button
const togglePassword = document.querySelector("#togglePassword");
const password = document.querySelector("#floatingPassword");

togglePassword.addEventListener("click", function () {
  // toggle the type attribute
  const type =
    password.getAttribute("type") === "password" ? "text" : "password";
  password.setAttribute("type", type);

  // toggle the eye icon
  const togglePasswordbtn = document.querySelector("#togglePasswordbtn");

  togglePasswordbtn.classList.toggle("fa-eye");
  togglePasswordbtn.classList.toggle("fa-eye-slash");
});

// Confirm Password Toggle Button
const toggleConfirmPassword = document.querySelector("#toggleConfirmPassword");
const Confirmpassword = document.querySelector("#floatingConfirmPassword");

toggleConfirmPassword.addEventListener("click", function () {
  // toggle the type attribute
  const type =
    Confirmpassword.getAttribute("type") === "password" ? "text" : "password";
  Confirmpassword.setAttribute("type", type);

  // toggle the eye icon
  const toggleConfirmPasswordbtn = document.querySelector(
    "#toggleConfirmPasswordbtn"
  );

  toggleConfirmPasswordbtn.classList.toggle("fa-eye");
  toggleConfirmPasswordbtn.classList.toggle("fa-eye-slash");
});
