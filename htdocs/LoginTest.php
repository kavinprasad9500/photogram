<?php
include 'libs/load.php';

// $user = "kavin";
// $pass = "Kavinprasad333@";
// $result = null;

// if (isset($_GET['logout'])) {
//     Session::destroy();
//     die("Session destroyed, <a href='test.php'>Login Again</a>");
// }

// if (Session::get('is_loggedin')) {
//     $userdata = Session::get('session_username');
//     $userobj = new User($user);
//     print("Welcome Back " . $userobj->getFirstname());
//     print("<br>" . $userobj->getBio());
// } else {
//     $result = User::login($user, $pass);
//     printf("No session found, Login Success. <br>");

//     if ($result) {
//         $userobj = new User($user);
//         echo "Login Success ", $userobj->getFirstname();
//         Session::set('is_loggedin', true);
//         Session::set('session_username', $result);
//     } else {
//         echo "Login failed, $user <br>";
//     }
// }
// echo <<<EOL
// <br><br><a href="test.php?logout">Logout</a>
// EOL;

$user = "kavin";

if (isset($_GET['logout'])) {
    if (Session::isset("session_token")) {
        $Session = new UserSession(Session::get("session_token"));
        if ($Session->removeSession()) {
            echo "<h3> Pervious Session is removing from db </h3>";
        } else {
            echo "<h3>Pervious Session not removing from db </h3>";
        }
    }
    Session::destroy();
    die("Session destroyed, <a href='logintest2.php'>Login Again</a>");
}

/*
1. Check if session_token in PHP session is available
2. If yes, construct UserSession and see if its successful.
3. Check if the session is valid one
4. If valid, print "Session validated"
5. Else, print "Invalid Session" and ask user to login.
*/

if (Session::isset("session_token")) {
    if (UserSession::authorize(Session::get("session_token"))) {
        echo "<h2>Session Login, WELCOME $user </h2>";
    } else {
        Session::destroy();
        die("<h2>Invalid Session, <a href='logintest2.php'>Login Again</a></h2>");
    }
} else {
    $pass = isset($_GET['pass']) ? $_GET['pass'] : '';
    if (!$pass) die("<h2>Password  is Empty</h2>");
    if (UserSession::authenticate($user, $pass)) {
        echo "<h2>New LOGIN Success,  WELCOME $user</h2>";
    } else echo "<h2>New Login Failed! $user</h2>";
}

echo <<<EOL
<br><br><Button><a href="logintest2.php?logout">Logout</a></Button>
EOL;
