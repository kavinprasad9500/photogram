<?php
include 'libs/load.php';
?>
<!DOCTYPE html>
<html lang="en">

<?php
    load_template('_head');
    load_template('_bgtheme');
?>

<body class="vw-100 vh-100 h-100 text-center text-white bg-dark-gradient sm-cover">
    <?php
        load_template('_forgot');
        load_template('_footer');
        load_template('_script');
    ?>
</body>

</html>